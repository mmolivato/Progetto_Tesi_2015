# Progetto Tesi Matteo Olivato

Algoritmi di ordinamento e il problema del consumo energetico.

Nella cartella 'Algoritmi' si trovano gli algoritmi in C utilizzati per i test presentati all'interno della Tesi.
Sono stati compilati per la Raspberry Pi model B utilizzando flag di compilazione per l'ottimizzazione quale -O3.
I file eseguibili hanno estenzione '.x'.

Nel file [test_ubuntu_i7_3537U.txt](https://gitlab.com/mmolivato/Progetto_Tesi_2015/blob/master/Algoritmi/For_Pi/mws/test_ubuntu_i7_3537U.txt) abbiamo i risultati ottenuti
nei tempi di esecuzione dal confronto a vari livelli di ottimizzazione tra [Cyclesort](https://en.wikipedia.org/wiki/Cycle_sort) e OracleCycleSort (nostra proposta).